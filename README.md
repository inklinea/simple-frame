# Simple Frame

Simple Frame - Create frames around selected objects - Inkscape 1.1+

Appears Under Extensions>Render>Simple Frame

▶ User choice of margin units
▶ Style can be copied from
  last selected object
▶ Inkscape 1.1+

#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Simple Frame - Create frames around selected objects
# An Inkscape 1.1+ extension
##############################################################################

import inkex
from inkex import PathElement, Style

from inklinea import Inklin

import uuid, os, copy, sys, shutil

def get_line_style(self, element):

    line_style = element.style

    line_style_copy = line_style.copy()
    line_style_temp = line_style.copy()

    for item in line_style_temp:
        if 'marker' in item:
            del line_style_copy[item]

    return line_style_copy

def dict_entry_to_variables(self, dict_entry, element_dict):
    # Unit conversion factor
    cf = self.cf

    x1 = element_dict[dict_entry]['x1']
    y1 = element_dict[dict_entry]['y1']
    x3 = element_dict[dict_entry]['x3']
    y2 = element_dict[dict_entry]['y2']

    width = element_dict[dict_entry]['width'] / cf
    height = element_dict[dict_entry]['height'] / cf

    x1 = float(x1 / cf)
    y1 = float(y1 / cf)
    x3 = float(x3 / cf)
    y2 = float(y2 / cf)

    return x1, y1, x3, y2, width, height

def element_dict_to_box_paths(self, parent, bbox_dict, selection_list, margins_dict, style_dict):
    # Unit conversion factor
    cf = self.cf

    layer_prefix = 'border' + '_frames'

    frames_layer = Inklin.create_new_group(self, parent, layer_prefix, 'layer', suffix_type='random')

    for item in selection_list:

        item_id = item.get_id()

        current_entry = dict_entry_to_variables(self, item_id, bbox_dict)

        x1, y1, x3, y2, width, height = current_entry

        x1 -= margins_dict['left']
        x3 += margins_dict['right']
        y1 -= margins_dict['top']
        y2 += margins_dict['bottom']

        # Build path box string
        d = f'M {x1} {y1} {x1} {y2} {x3} {y2} {x3} {y1} z'

        my_path = PathElement()
        my_path.set('d', d)
        my_path.style = style_dict

        frames_layer.append(my_path)

    parent.append(frames_layer)

    return frames_layer


class SimpleFrame(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--main_notebook", type=str, dest="main_notebook", default=0)

        pars.add_argument("--simple_top_margin", type=float, dest="simple_top_margin", default=1)
        pars.add_argument("--simple_left_margin", type=float, dest="simple_left_margin", default=1)
        pars.add_argument("--simple_bottom_margin", type=float, dest="simple_bottom_margin", default=1)
        pars.add_argument("--simple_right_margin", type=float, dest="simple_right_margin", default=1)
        pars.add_argument("--simple_units_combo", type=str, dest="simple_units_combo", default='px')

        pars.add_argument("--bbox_type_radio", type=str, dest="bbox_type_radio", default='visual')

        pars.add_argument("--style_from_radio", type=str, dest="style_from_radio", default='visual')
        pars.add_argument("--last_object_cb", type=str, dest="last_object_cb", default='true')

        # simple style page
        pars.add_argument("--simple_fill_cb", type=str, dest="simple_fill_cb")
        pars.add_argument("--simple_fill_cp", type=inkex.colors.Color, dest="simple_fill_cp")

        pars.add_argument("--simple_stroke_cb", type=str, dest="simple_stroke_cb")
        pars.add_argument("--simple_stroke_cp", type=inkex.colors.Color, dest="simple_stroke_cp")

        pars.add_argument("--simple_stroke_width_float", type=int, dest="simple_stroke_width_float", default=1)
        pars.add_argument("--simple_stroke_units_combo", type=str, dest="simple_stroke_units_combo", default='px')

    def effect(self):

        self.cf = Inklin.conversions[self.svg.unit]

        unit_choice = self.options.simple_units_combo

        self.margin_cf = margin_cf = Inklin.conversions[unit_choice] / Inklin.conversions[self.svg.unit]

        parent = self.svg

        selection_list = self.svg.selected

        if len(selection_list) < 1:
            inkex.errormsg('Nothing Selected')
            return

        margins_dict = {}
        margins_dict['top'] = self.options.simple_top_margin * margin_cf
        margins_dict['left'] = self.options.simple_left_margin * margin_cf
        margins_dict['bottom'] = self.options.simple_bottom_margin * margin_cf
        margins_dict['right'] = self.options.simple_right_margin * margin_cf


        if self.options.style_from_radio == 'last_object':
            style_object = get_line_style(self, selection_list[-1])
            # If only one object selected use it's style
            # Else copy style from last object, but do not frame last object
            if len(selection_list) > 1:
                if self.options.last_object_cb == 'false':
                    selection_list.pop(-1)

        else:
            style_object = Style()
            if self.options.simple_stroke_cb == 'true':
                style_object['stroke'] = self.options.simple_stroke_cp
            else:
                style_object['stroke'] = 'none'
            if self.options.simple_fill_cb == 'true':
                style_object['fill'] = self.options.simple_fill_cp
            else:
                style_object['fill'] = 'none'
            # Make a 0.5mm stroke

            stroke_units = self.options.simple_stroke_units_combo
            stroke_width = self.options.simple_stroke_width_float
            stroke_unit_cf = Inklin.conversions[stroke_units] / Inklin.conversions[self.svg.unit]
            style_object['stroke-width'] = str(stroke_width * stroke_unit_cf)


        # Command call to get all bounding boxes

        if self.options.bbox_type_radio == 'visual':
            temp_svg_filepath = self.options.input_file


        elif self.options.bbox_type_radio == 'geometric':

            temp_svg = copy.deepcopy(self.svg)

            for element in selection_list:
                element_id = element.get_id()
                temp_element = temp_svg.getElementById(element_id)
                temp_element.style = ''

            self.temp_folder = temp_folder = Inklin.make_temp_folder(self)
            temp_svg_filename = str(uuid.uuid4()) + '.svg'
            temp_svg_filepath = os.path.join(temp_folder, temp_svg_filename)

            with open(temp_svg_filepath, 'w') as query_temp_file:
                my_svg_string = temp_svg.root.tostring().decode("utf-8")
                query_temp_file.write(my_svg_string)
                query_temp_file.close()

        bbox_dict = Inklin.inkscape_command_call_bboxes_to_dict(self, temp_svg_filepath)

        element_dict_to_box_paths(self, parent, bbox_dict, selection_list, margins_dict, style_object)


        # Cleanup temp folder
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)


if __name__ == '__main__':
    SimpleFrame().run()
